# Projet chuchut

Projet de réduction de volume pour une sortie analogique pour casque.
En collaboration avec le humanlab Saint-Pierre

## Répertoires

* cao: fichiers d'impression du boitier
* kicad: dossier de conception électronique de la carte de réduction de volume
* ltspice: différentes solutions de simulation LTspice

## License

[Boitier Chuchut](https://gitlab.inria.fr/humanlab-inria/chutchut), copyright 2022 by [Jeremy Blanquinque](https://gitlab.inria.fr/jblanqui) and [Roger Pissard-Gibollet](https://gitlab.inria.fr/pissard) / [Humanlab Inria](https://www.inria.fr) is licensed under  Attribution-NonCommercial-ShareAlike 4.0 International [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![logo cc by-nc-sa](./cc_by-nc-sa.png)

